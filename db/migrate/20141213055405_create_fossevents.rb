class CreateFossevents < ActiveRecord::Migration
  def change
    create_table :fossevents do |t|
    	t.text :title
    	t.date   :from_date
    	t.date   :to_date
    	t.string :event_type
    	t.text :description
    	t.timestamps
    end
  end
end
