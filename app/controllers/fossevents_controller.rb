class FosseventsController < ApplicationController
	def index
		@events = Fossevent.all
	end

	def new
		@event = Fossevent.new
	end

	def create
		Fossevent.create(create_params)
		redirect_to action: 'index'
	end

	def show 
	    # render html: "hkhkj"
	    @event =Fossevent.find(params[:id])

    end 


	private
	def create_params
		params.require(:fossevent).permit(:title, :from_date, :to_date, :event_type)
	end
end